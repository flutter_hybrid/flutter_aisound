package com.example.flutter_aisound;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.asr_plugin.asr.AsrPlugin;

//import io.flutter.app.FlutterActivity;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.embedding.engine.plugins.shim.ShimPluginRegistry;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {

    @Override
    public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
        super.configureFlutterEngine(flutterEngine);
        GeneratedPluginRegistrant.registerWith(flutterEngine);

        ShimPluginRegistry shimPluginRegistry = new ShimPluginRegistry(flutterEngine);

        registerCustomPlugin(shimPluginRegistry);
    }

    private static void registerCustomPlugin(PluginRegistry registry) {

        AsrPlugin.registerWith(registry.registrarFor("com.example.asr_plugin.asr.AsrPlugin"));

    }
}
